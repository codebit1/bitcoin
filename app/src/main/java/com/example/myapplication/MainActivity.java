package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button bAdd;
    EditText etCountry, etRate, etCurrency, etDescription;
    boolean isAllFieldsChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bAdd = findViewById(R.id.addButton);

        etCountry = findViewById(R.id.editCountry);
        etRate = findViewById(R.id.editRate);
        etCurrency = findViewById(R.id.editCurrency);
        etDescription = findViewById(R.id.editDescription);

        bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAllFieldsChecked = CheckAllFields();
                if (isAllFieldsChecked) {
                    Intent i = new Intent(MainActivity.this, MainActivity2.class);
                    startActivity(i);
                }
            }
        });

    }

    private boolean CheckAllFields() {
        if (etCountry.length() == 0) {
            etCountry.setError("This Field is Required");
            return false;
        }
        if (etRate.length() == 0) {
            etRate.setError("This Field is Required");
            return false;
        }
        if (etCurrency.length() == 0) {
            etCurrency.setError("This Field is Required");
            return false;
        }
        if (etDescription.length() == 0) {
            etDescription.setError("This Field is Required");
            return false;
        }
        return  true;
    }


}